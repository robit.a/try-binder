# try-binder
#### Alec Robitaile

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/robit.a%2Ftry-binder/master?urlpath=rstudio)

### Requirements

See [Binder Examples - R](https://github.com/binder-examples/r).

1. `runtime.txt` - R version. 
2. `install.R` - R packages
3. `apt.txt` - install programs eg. GEOS
4. `requirements.txt` - python packages


### Notes

* R 4.0 not supported atm. 
* Holepunch doesn't have GitLab support atm. 
* Binders can have "up to 100 simulatenous users" 
* Zip folders to download in one shot all of a user's files, outputs, etc
* Don't include `renv` setup, but include `renv`


### Resources

* https://mybinder.org/
* https://karthik.github.io/holepunch/index.html
* https://discourse.jupyter.org/t/how-to-reduce-mybinder-org-repository-startup-time/4956
